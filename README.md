# Harmony Admin Theme

## Using SASS

We use Bundler to manage our Gem dependencies and make sure our SASS compilation is consistent, usage is simple:

```
$ # Install bundler
$ gem install bundler
$ # Install project's dependencies, listed in Gemfile
$ bundle install
$ # Compile SASS changes
$ bundle exec compass compile
